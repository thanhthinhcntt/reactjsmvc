﻿const mix = require('laravel-mix');

mix.options({
    processCssUrls: true,
    clearConsole: true,
    terser: {
        extractComments: false,
    },
    manifest: false,
});

mix.webpackConfig({
    stats: {
        children: false,
    },
    externals: {
        react: 'React',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
});


mix.disableSuccessNotifications();

mix.react();
mix.setPublicPath('Scripts');

mix.js('assets/js/DemoComponent.tsx', 'Scripts/demo').react(); 
