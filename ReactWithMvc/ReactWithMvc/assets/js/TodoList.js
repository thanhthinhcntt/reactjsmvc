﻿import React, { useState } from 'react';

export const TodoList = () => {
    const [todos, setTodos] = useState([]); // State để lưu trữ danh sách các todo

    const [newTodo, setNewTodo] = useState(''); // State để lưu trữ giá trị của input

    const handleInputChange = (event) => {
        setNewTodo(event.target.value); // Cập nhật giá trị mới khi người dùng nhập vào input
    };

    const handleAddTodo = () => {
        if (newTodo.trim()) {
            setTodos([...todos, newTodo]); // Thêm một todo mới vào danh sách
            setNewTodo(''); // Đặt lại giá trị của input thành rỗng
        }
    };

    const handleDeleteTodo = (index) => {
        const newTodos = [...todos]; // Tạo một bản sao mới của danh sách todos
        newTodos.splice(index, 1); // Xóa một todo khỏi danh sách
        setTodos(newTodos); // Cập nhật state với danh sách mới
    };

    return (
        <div>
            <h2>Todo List</h2>
            <input
                type="text"
                value={newTodo}
                onChange={handleInputChange}
                placeholder="Enter a new todo"
            />
            <button onClick={handleAddTodo}>Add Todo</button>
            <ul>
                {todos.map((todo, index) => (
                    <li key={index}>
                        {todo}
                        <button onClick={() => handleDeleteTodo(index)}>Delete</button>
                    </li>
                ))}
            </ul>
        </div>
    );
};
