﻿import React from 'react';
import { createRoot } from 'react-dom/client'; 
import { TodoList } from './TodoList';
export const DemoUI = () => {
    return (
        <div>Hello from React Chó Nhân</div>
    )
}


const root = createRoot(document.getElementById('reactApp'));


root.render(<>
    <DemoUI />
    <TodoList />
</>);
